#![allow(unused)]

fn main() {
    const MAX_POINTS: u32 = 100_000;
    let mut x = 5;
    println!("The value of x is : {}", x);
    x = 6;
    println!("The value of x is : {}\n", x);

    let x_addr = &x;

    let x = x + 1;
    let x = x * 2;

    println!("The value of x is : {}", x);
    println!("The value of x is : {}", x_addr);
}
